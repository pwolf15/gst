#include <gst/gst.h>

/* Structure to contain all our information, so we can pass it around */
typedef struct _CustomData {
    GstElement *playbin;    /* Our one and only element */
    gboolean playing;       /* Are we in the PLAYING state? */
    gboolean terminate;     /* Should we terminate execution? */
    gboolean seek_enabled;  /* Is seeking enabled for this media? */
    gboolean seek_done;     /* Have we performed seek */
    gint64 duration;        /* How long does this media last, in nanoseconds */
} CustomData;

/* Forward definition of the message processing function */
static void handle_message(CustomData *data, GstMessage *msg);

int main(int argc, char** argv)
{
    CustomData data;
    GstBus* bus;
    GstMessage* msg;
    GstStateChangeReturn ret;

    data.playing = FALSE;
    data.terminate = FALSE;
    data.seek_enabled = FALSE;
    data.seek_done = FALSE;
    data.duration = GST_CLOCK_TIME_NONE;

    /* Initialize GStreamer */
    gst_init(&argc, &argv);

    /* Create the elements */
    data.playbin = gst_element_factory_make ("playbin", "playbin");

    if (!data.playbin) {
        g_printerr ("Not all elements could be created.\n");
        return -1;
    }

    /* Set the URI to play */
    g_object_set (data.playbin, "uri", "file:///home/pwolf/dev/gst/pujols.mp4", NULL);

    /* Start playing */
    ret = gst_element_set_state(data.playbin, GST_STATE_PLAYING);
    if (ret == GST_STATE_CHANGE_FAILURE) {
        g_printerr ("Unable to set pipeline to playing state.\n");
        gst_object_unref(data.playbin);
        return -1;
    }

    bus = gst_element_get_bus(data.playbin);

    do {

        msg = gst_get_bus_pop_filtered(bus, GST_CLOCK_TIME_MS * 100, 
            GST_MESSAGE_STATE_CHANGED | GST_MESSAGE_EOS | GST_MESSAGE_ERROR | GST_MESSAGE | GST_MESSAGE_DURATION);

        /* Parse message */
        if (msg != NULL) {
            handle_message (&data, msg);
        } else {
            /* We got no message, timeout has expired */
            if (data.playing) {
                gint64 current = -1;

                /* Query the current position of the stream */
                if (!gst_element_query_position(data.playbin, GST_FORMAT_TIME, &current)) {
                    g_printerr("Could not query current position.\n");
                }

                /* If we didn't know it yet, query the stream duration */
                if (!GST_CLOCK_TIME_IS_VALID (data.duration)) {
                    if (!gst_element_query_duration(data.playbin, GST_FORMAT_TIME, &data.duration)) {
                        g_printerr("Could not query current duration.\n");
                    }
                }
            }
        }

    } while (!data.terminate);

    /* Free resources */
    gst_object_unref(bus);
    gst_element_set_state(data.playbin, GST_STATE_NULL);
    gst_object_unref(data.playbin);

    return 0;
}

static void handle_message (CustomData *data, GstMessage *msg) {

}