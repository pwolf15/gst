#include <gst/gst.h>
#include <gst/app/gstappsrc.h>
#include <stdlib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// #include <gdk-pixbuf/gdk-pixbuf.h>

GST_DEBUG_CATEGORY(appsrc_pipeline_debug);
#define GST_CAT_DEFAULT appsrc_pipeline_debug

typedef struct _App App;

struct _App
{
    GstElement *pipeline;
    GstElement *appsrc;

    GMainLoop *loop;
    guint sourceid;

    GTimer *timer;
};

App s_app;

static int intensity = 0;
static int sampleNum = 0;

static gboolean
read_data(App *app)
{
    guint len;
    GstFlowReturn ret;
    GstMapInfo map;
    gdouble ms;
    gint8 *raw;

    int rendered = 0;
    // printf("read_data\n");
    do
    {

        ms = g_timer_elapsed(app->timer, NULL);
        int rgb[3] = { 0, 0, 0};
        rgb[0] = rand() % 256;
        rgb[1] = rand() % 256;
        rgb[2] = rand() % 256;
        if (ms > 1.0 / 20.0)
        {
            rendered = 1;
            GstBuffer *buffer;
            buffer = gst_buffer_new_and_alloc(1280 * 1024 * 3);
            gst_buffer_map(buffer, &map, GST_MAP_WRITE);

            raw = (gint8 *)map.data;
            // GdkPixbuf *pb;
            gboolean ok = TRUE;

            for (int i = 0; i < 1280 * 1024; i++)
            {
                if (i % 4 == 0) {
                    raw[i * 3] = rgb[0];
                    raw[i * 3 + 1] = rgb[1];
                    raw[i * 3 + 2] = rgb[2];
                }

            }

            // pb = gdk_pixbuf_new(GDK_COLORSPACE_RGB, FALSE, 8, 640, 480);
            // gdk_pixbuf_fill(pb, 0xffffffff);

            // GST_BUFFER_DATA (buffer) = gdk_pixbuf_get_pixels(pb);
            // GST_BUFFER_SIZE (buffer) = 640*480*3*sizeof(guchar);
            /* Set its timestamp and duration */
            sampleNum++;
            GST_BUFFER_TIMESTAMP (buffer) = gst_util_uint64_scale (sampleNum++, GST_SECOND, 20);
            GST_BUFFER_DURATION (buffer) = 0.05; //gst_util_uint64_scale (sampleNum++, GST_SECOND, 20);

            GST_DEBUG("feed buffer");
            gst_buffer_unmap(buffer, &map);
            g_signal_emit_by_name(app->appsrc, "push-buffer", buffer, &ret);
            gst_buffer_unref(buffer);

            if (ret != GST_FLOW_OK)
            {
                /* some error, stop sending data */
                GST_DEBUG("some error");
                ok = FALSE;
            }

            g_timer_start(app->timer);

            return ok;
        }
    } while (!rendered);

    //g_signal_emit_by_name (app->appsrc, "end-of-stream", &ret);
    return FALSE;
}

/* This signal callback is called when appsrc needs data, we add an idle handler
 * to the mainloop to start pushing data into the appsrc */
static void
start_feed(GstElement *pipeline, guint size, App *app)
{
    if (app->sourceid == 0)
    {
        GST_DEBUG("start feeding");
        app->sourceid = g_idle_add((GSourceFunc)read_data, app);
    }
}

/* This callback is called when appsrc has enough data and we can stop sending.
 * We remove the idle handler from the mainloop */
static void
stop_feed(GstElement *pipeline, App *app)
{
    if (app->sourceid != 0)
    {
        GST_DEBUG("stop feeding");
        g_source_remove(app->sourceid);
        app->sourceid = 0;
    }
}

static gboolean
bus_message(GstBus *bus, GstMessage *message, App *app)
{
    GST_DEBUG("got message %s",
              gst_message_type_get_name(GST_MESSAGE_TYPE(message)));

    switch (GST_MESSAGE_TYPE(message))
    {
    case GST_MESSAGE_ERROR:
    {
        GError *err = NULL;
        gchar *dbg_info = NULL;

        gst_message_parse_error(message, &err, &dbg_info);
        g_printerr("ERROR from element %s: %s\n",
                   GST_OBJECT_NAME(message->src), err->message);
        g_printerr("Debugging info: %s\n", (dbg_info) ? dbg_info : "none");
        g_error_free(err);
        g_free(dbg_info);
        g_main_loop_quit(app->loop);
        break;
    }
    case GST_MESSAGE_EOS:
        g_main_loop_quit(app->loop);
        break;
    default:
        break;
    }
    return TRUE;
}

int main(int argc, char *argv[])
{
    App *app = &s_app;
    GError *error = NULL;
    GstBus *bus;
    GstCaps *caps;

    gst_init(&argc, &argv);

    GST_DEBUG_CATEGORY_INIT(appsrc_pipeline_debug, "appsrc-pipeline", 0,
                            "appsrc pipeline example");

    /* create a mainloop to get messages and to handle the idle handler that will
   * feed data to appsrc. */
    app->loop = g_main_loop_new(NULL, TRUE);
    app->timer = g_timer_new();
    g_timer_start(app->timer);

    app->pipeline = gst_parse_launch("appsrc name=mysource ! video/x-raw,format=RGB,width=1280,height=1024,bpp=24,depth=24 ! videoconvert ! autovideosink", NULL);
    g_assert(app->pipeline);

    bus = gst_pipeline_get_bus(GST_PIPELINE(app->pipeline));
    g_assert(bus);

    /* add watch for messages */
    gst_bus_add_watch(bus, (GstBusFunc)bus_message, app);

    /* get the appsrc */
    app->appsrc = gst_bin_get_by_name(GST_BIN(app->pipeline), "mysource");
    g_assert(app->appsrc);
    // g_assert(GST_IS_APP_SRC(app->appsrc));
    g_signal_connect(app->appsrc, "need-data", G_CALLBACK(start_feed), app);
    g_signal_connect(app->appsrc, "enough-data", G_CALLBACK(stop_feed), app);

    /* set the caps on the source */
    caps = gst_caps_new_simple("video/x-raw",
                               "format", G_TYPE_STRING, "RGB",
                               "bpp", G_TYPE_INT, 24,
                               "depth", G_TYPE_INT, 24,
                               "width", G_TYPE_INT, 1280,
                               "height", G_TYPE_INT, 1024,
                               NULL);
    g_object_set(app->appsrc, "caps", caps, "format", GST_FORMAT_TIME, NULL);

    /* go to playing and wait in a mainloop. */
    gst_element_set_state(app->pipeline, GST_STATE_PLAYING);

    /* this mainloop is stopped when we receive an error or EOS */
    g_main_loop_run(app->loop);

    GST_DEBUG("stopping");

    gst_element_set_state(app->pipeline, GST_STATE_NULL);

    gst_object_unref(bus);
    g_main_loop_unref(app->loop);

    return 0;
}